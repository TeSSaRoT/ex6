package com.example.itryex4

import com.google.gson.annotations.SerializedName

data class BelarusBankATM(
    @SerializedName("gps_x") val gps_x: Double,
    @SerializedName("gps_y") val gps_y: Double,
    var distance:Double = 0.0
)