package com.example.itryex4

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.itryex4.databinding.ActivityMapsBinding
import com.example.itryex4.interfaces.AppComponent
import com.example.itryex4.interfaces.DaggerAppComponent
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import javax.inject.Inject


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private val appComponent: AppComponent = DaggerAppComponent.create()
    @Inject
    lateinit var pointsBelarusBankATM:PointsBelarusBankATM
    private val model: MapViewModel by viewModels()
    private val gomelXY = LatLng(52.4311882, 30.994018)
    private val pointXY = LatLng(52.425163, 31.015039)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gomelXY, 10.0F))
        mMap.addMarker(MarkerOptions().position(pointXY))
        val gomel = resources.getString(R.string.gomel)

        val mapObserver = Observer<BelarusBankATM> {
            mMap.addMarker(
                MarkerOptions().position(
                    LatLng(
                        it.gps_x,
                        it.gps_y
                    )
                )
            )
        }

        model.mapLiveData.observe(this, mapObserver)
        appComponent.inject(this)
        model.getNPoint(10,gomel,pointXY,pointsBelarusBankATM)
    }

    override fun onDestroy() {
        model.mapLiveData.removeObservers(this)
        super.onDestroy()
    }
}