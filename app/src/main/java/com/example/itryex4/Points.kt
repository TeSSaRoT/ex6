package com.example.itryex4

import android.location.Location
import com.example.itryex4.interfaces.BuildRetrofit
import com.example.itryex4.interfaces.FindDistance
import com.example.itryex4.interfaces.IPoints
import com.google.android.gms.maps.model.LatLng
import dagger.Module
import dagger.Provides
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers


@Module
class PointsBelarusBankATM: FindDistance, BuildRetrofit, IPoints {

    private lateinit var retrofit:BelarusbankPointsAPI

    @Provides
    fun providePointsBelarusBankATM():PointsBelarusBankATM{
        return PointsBelarusBankATM()
    }

    override fun constructRetrofit(retrofitModule:RetrofitModule,baseUrl:String,levelBODY:String){
        this.retrofit = retrofitModule.provideCreateRetrofitBelarusBank(levelBODY,baseUrl)
    }

    private fun getAtm(city:String): Single<List<BelarusBankATM>> {
        return retrofit.getAtmByCity(city)
    }

    private fun getInfobox(city:String): Single<List<BelarusBankATM>> {
        return retrofit.getInfoboxByCity(city)
    }

    private fun getFilials(city:String): Single<List<BelarusBankATM>> {
        return retrofit.getFilialsInfoByCity(city)
    }

    override fun zipPoints(point1: Single<List<BelarusBankATM>>,
                           point2: Single<List<BelarusBankATM>>,
    ): Single<List<BelarusBankATM>> {
        return Single.zip(
            point1,
            point2,
                { fp, sp -> fp + sp}
            ).subscribeOn(Schedulers.io())
    }

    override fun sortPointsByDistance(responseList: Single<List<BelarusBankATM>>, pointXY: LatLng): Single<List<BelarusBankATM>>{
        return responseList.map { list ->
            list.forEach { elem ->
                elem.distance = getDistance(LatLng(elem.gps_x, elem.gps_y), pointXY)
            }
            list.sortedWith(compareBy({ it.distance }, { it.gps_x }, { it.gps_y }))
        }.subscribeOn(Schedulers.computation())
    }

    override fun getZipAndSorted(city:String, pointXY: LatLng ): Single<List<BelarusBankATM>>{
        return sortPointsByDistance(
            zipPoints(
                zipPoints(getAtm(city), getInfobox(city)),
                getFilials(city)
            ),
            pointXY
        )
    }

    override fun getDistance(pointFrom: LatLng, pointTo: LatLng): Double {
        val startLocation = Location("startLocation")
        startLocation.latitude = pointTo.latitude
        startLocation.longitude = pointTo.longitude
        val pointLocation = Location("pointLocation")
        pointLocation.latitude = pointFrom.latitude
        pointLocation.longitude = pointFrom.longitude
        return startLocation.distanceTo(pointLocation).toDouble()
    }
}