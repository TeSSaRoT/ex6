package com.example.itryex4

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.itryex4.interfaces.AppComponent
import com.example.itryex4.interfaces.DaggerAppComponent
import com.example.itryex4.interfaces.MapViewModelImpl
import com.google.android.gms.maps.model.LatLng
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

const val baseUrl = "https://belarusbank.by/api/"
const val LevelBODY = "BODY"

class MapViewModel:ViewModel(), MapViewModelImpl {

    @Inject
    lateinit var retrofitModule:RetrofitModule

    private val appComponent: AppComponent = DaggerAppComponent.create()

    val mapLiveData: MutableLiveData<BelarusBankATM> by lazy {
        MutableLiveData<BelarusBankATM>()
    }

    override fun getNPoint(count: Int, city:String, point:LatLng, pointsBelarusBankATM:PointsBelarusBankATM){
        appComponent.inject(this)
        pointsBelarusBankATM.constructRetrofit(retrofitModule,baseUrl,LevelBODY)
        pointsBelarusBankATM
            .getZipAndSorted(city,point)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ list ->
                list.subList(0,count).forEach {
                mapLiveData.value = it
                }
            },
                {

                })
    }
}