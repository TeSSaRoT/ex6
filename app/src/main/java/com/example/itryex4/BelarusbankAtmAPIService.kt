package com.example.itryex4

import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BelarusbankPointsAPI {

    @GET("atm")
    fun getAtmByCity(@Query("city") city: String): Single<List<BelarusBankATM>>

    @GET("infobox")
    fun getInfoboxByCity(@Query("city") city: String): Single<List<BelarusBankATM>>

    @GET("filials_info")
    fun getFilialsInfoByCity(@Query("city") city: String): Single<List<BelarusBankATM>>
}