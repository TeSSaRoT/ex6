package com.example.itryex4

import com.example.itryex4.interfaces.RetrofitImpl
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class RetrofitModule: RetrofitImpl {

    @Provides
    fun provideRetrofit():RetrofitModule{
        return RetrofitModule()
    }

    override fun provideOkHttpClient(level:String): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.valueOf(level.uppercase())))
            .build()


    override fun provideBuildRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .baseUrl(url)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .build()
    }

    fun provideCreateRetrofitBelarusBank(level:String, url: String):BelarusbankPointsAPI {
        return provideBuildRetrofit(
            provideOkHttpClient(level),
            url)
            .create(BelarusbankPointsAPI::class.java)
    }
}