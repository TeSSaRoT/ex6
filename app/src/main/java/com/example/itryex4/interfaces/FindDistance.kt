package com.example.itryex4.interfaces

import com.google.android.gms.maps.model.LatLng

interface FindDistance {

    fun getDistance(pointFrom: LatLng, pointTo: LatLng): Double
}