package com.example.itryex4.interfaces

import com.example.itryex4.RetrofitModule

interface BuildRetrofit {
    fun constructRetrofit(retrofitModule: RetrofitModule, baseUrl:String,levelBODY:String)
}