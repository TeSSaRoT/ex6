package com.example.itryex4.interfaces

import com.example.itryex4.BelarusBankATM
import com.google.android.gms.maps.model.LatLng
import io.reactivex.rxjava3.core.Single

interface IPoints {
    fun zipPoints(point1: Single<List<BelarusBankATM>>,
                  point2: Single<List<BelarusBankATM>>,
    ): Single<List<BelarusBankATM>>

    fun sortPointsByDistance(responseList: Single<List<BelarusBankATM>>, pointXY: LatLng): Single<List<BelarusBankATM>>

    fun getZipAndSorted(city:String, pointXY: LatLng ): Single<List<BelarusBankATM>>
}