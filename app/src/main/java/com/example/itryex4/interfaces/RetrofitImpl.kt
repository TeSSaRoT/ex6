package com.example.itryex4.interfaces

import okhttp3.OkHttpClient
import retrofit2.Retrofit

interface RetrofitImpl {

    fun provideOkHttpClient(level:String): OkHttpClient

    fun provideBuildRetrofit(okHttpClient: OkHttpClient, url: String): Retrofit
}