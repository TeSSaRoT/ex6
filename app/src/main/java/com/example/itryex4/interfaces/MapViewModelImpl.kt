package com.example.itryex4.interfaces

import com.example.itryex4.PointsBelarusBankATM
import com.google.android.gms.maps.model.LatLng

interface MapViewModelImpl {
    fun getNPoint(count: Int, city:String, point: LatLng, pointsBelarusBankATM: PointsBelarusBankATM)
}