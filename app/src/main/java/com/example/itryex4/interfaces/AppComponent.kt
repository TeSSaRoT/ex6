package com.example.itryex4.interfaces

import com.example.itryex4.MapViewModel
import com.example.itryex4.MapsActivity
import com.example.itryex4.PointsBelarusBankATM
import com.example.itryex4.RetrofitModule
import dagger.Component

@Component(modules = [RetrofitModule::class, PointsBelarusBankATM::class])
interface AppComponent {
    fun inject(mapViewModel: MapViewModel)
    fun inject(mapsActivity: MapsActivity)
}